FROM node:16.8.0
RUN apt-get update && apt-get install -y git vim bash wget npm
ADD http://worldtimeapi.org/api/ip time.json
RUN git clone https://gitlab.com/jean-tinyurl/client.git

WORKDIR /client
RUN npm install && npm run build

WORKDIR /
RUN git clone https://gitlab.com/jean-tinyurl/api.git

WORKDIR /api
RUN npm install && npm run tsc
RUN cp -fri ../client/build/* build/
RUN mv build/index.html .

EXPOSE 9000

CMD ["npm", "run", "start"]
