# Get Start with tinyUrl tools - Backend

## Framework
- Express
- Typescript
- Heroku

## Methods to start the project

### Preprocess:
- Install npm and docker
- Install git and set the gitlab account

### Start and Stop Project
1. `git clone` this project to your local
2. Run web
    - Choice 1: Run the Project Locally
        - `npm install`: Install the dependencies and generate the /node_modules
        - `npm run tsc`: Exchange ts to js and generate the /build
        - `npm run start`: Run api on `http://localhost:9000`
    - Choice 2: Run the Project in Docker Container
        - `docker build -t api .`: Build the docker image based on dockerfile
        - `docker run -it -p 9000:9000 --name api api`: Run the container based on the docker image
        - The service also run on `http://localhost:9000`

3. Start service: `ctrl + c`

### Notes:
1. The repo only includes backend part, please check frontend source codes in: `https://gitlab.com/jean-tinyurl/client`
2. The web also deploy to heroku: `https://tinyurl-jean.herokuapp.com/`
3. The api can test with [postman collection](https://www.getpostman.com/collections/c70656a65c1a37ca6674)

### Todo and Priorities
- [ ] `Deploy the Service to Heroku with Gitlab pipeline`: Permission and configure issue in heroku; keep fixing
- [ ] `Uinttest & E2E Test`: Need to revise the EC6 and commonJS setting; E2E may add in frontend repo with selinum or cypress triggering by gitlab lambda
- [ ] `Build up Cache`: Will simply deal with maps and clear depends on time and queue
- [ ] `User Management & Use Counts`: Already create table in heroku postgresql
