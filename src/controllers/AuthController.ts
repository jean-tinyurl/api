import {Request, Response} from "express";

export class AuthController {
    echo(req: Request, res: Response) {
        res.send("check");
    }
}

export class PingController {
    echo(req: Request, res: Response) {
        res.send("pong");
    }
}
