import { Request, Response } from "express";
import fetch from "node-fetch";
import { nanoid } from "nanoid";
import { Client } from "pg";
import request from "request";
import { Json } from "sequelize/types/lib/utils";
import jsdom from "jsdom";

async function getMeta(url: string){
    const fromServer = await fetch(url)
        .then(function(response:any) {
            return response.text();
        }).catch(error => console.log("error", error));

    const dom = new jsdom.JSDOM(fromServer).window.document;
    let result = dom.getElementsByTagName("title")[0].outerHTML;
    const metas = dom.getElementsByTagName("meta");

    for (let i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute("property") !== null) {
            result+=metas[i].outerHTML;
        }
    }
    return result;
}

async function exchangeUrl(url: string, createNew: boolean){
    const client = new Client({
        host: "ec2-54-147-126-173.compute-1.amazonaws.com",
        database: "dlaoqdtcehhia",
        port: 5432,
        user: "mtcfgkdfqzqqzg",
        password: "990ff4d01c59753699ecccbab72ed4775e2353a3eeb85f765f5db02709b6a634",
        ssl: {
            rejectUnauthorized: false
        }
    });

    client.connect();

    let text = "";
    let values = new Array(3);
    if(createNew == true){

        text = "INSERT INTO urls( original_url, short_url, created_on) VALUES($1, $2, to_timestamp($3 / 1000.0)) RETURNING *";
        values = [ url, nanoid(), Date.now()];
    }
    else{
        text = "SELECT original_url FROM urls WHERE short_url = $1";
        values = [url];
        console.log(url);
    }

    try {
        const res = await client.query (text, values);
        if (createNew == true){ return res.rows[0].short_url;}
        else{
            const target= res.rows[0].original_url;
            const meta = await getMeta(target);
            return `<!DOCTYPE html>
                <html>
                    <head>
                        ${meta}
                        <script>
                            window.onload = function() {
                                var target = document.getElementById('url').value;
                                if( target ){
                                    window.location.replace(target);
                                } else {
                                    console.log(target);
                                }
                            }
                        </script>
                    </head>
                    <body>
                        <form id="form" name="form" method="get" action="/">
                            <input type="hidden" id="url" name="url" value=${target}/>
                        </form>
                    </body>
                </html>`;
        }    
    } catch (err:any) {
        return err.stack;
    }
}

export default class TinyUrlController {
    echo(req: Request, res: Response) {
        request(req.body.original, function (error: Error, response: any, body: Json) {
            if (!error && response.statusCode >= 200 && response.statusCode < 400) {
                exchangeUrl(req.body.original, true).then(function(result) {
                    console.log(result);
                    res.send("https://tinyurl-jean.herokuapp.com/turl/" + result);
                }).catch((error)=>{console.log("ERROR URL SQL:",error);});
                
            }
            else {
                res.send("fail");
            }
        });
    }
    redirect(req: Request, res: Response) {
        if (/[A-Za-z0-9_-]{21}/.test(req.params.shortUrl)) {
            exchangeUrl(req.params.shortUrl, false).then(function(result) {
                console.log(result);
                res.send(result);
            }).catch((error)=>{console.log("ERROR TinyUrl SQL:", error);});
        }
        else {
            res.send("wrong tinyUrl");
        }
    }
}