import {check, param} from "express-validator";
import {showApiError} from "../middleware/AuthMiddleware";

export const tinyRequest = [
    check('original').exists().isURL({protocols:['https'], require_tld: true, require_protocol: true, require_host: true, require_port: false, require_valid_protocol: true, allow_underscores: false, allow_trailing_dot: false, allow_protocol_relative_urls: false, disallow_auth: false, validate_length: true}),
    showApiError
];

export const redirectRequest = [
    param('shortUrl').exists().isLength({min:21, max:21}),
    showApiError
];
