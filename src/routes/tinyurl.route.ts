import TinyUrlController from "../controllers/TinyUrlController";
import {tinyRequest, redirectRequest} from "../requests/TinyUrlRequest";
import Route from "./route";

class TinyUrlRoute extends Route{
  private tinyUrlController = new TinyUrlController();
  constructor() {
      super();
      this.setRoutes();
  }
  protected setRoutes() {
      this.router.post("/createTiny", tinyRequest, this.tinyUrlController.echo);
      this.router.get("/turl/:shortUrl", redirectRequest, this.tinyUrlController.redirect);
      // this.router.use(AuthMiddleware); for whole file
  }
}

export default TinyUrlRoute;