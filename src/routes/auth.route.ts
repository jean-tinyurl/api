import {AuthController, PingController} from "../controllers/AuthController"
import {AuthMiddleware} from "../middleware/AuthMiddleware"
import {loginRequest} from "../requests/AuthRequest"
import Route from "./route";

class AuthRoute extends Route{
  private authController = new AuthController();
  private pongController = new PingController();
  constructor() {
    super();
    this.prefix = '/auth';
    this.setRoutes();
  }
  protected setRoutes() {
    this.router.post('/login', loginRequest, AuthMiddleware, this.authController.echo);
    this.router.get('/logout', this.pongController.echo);
    // this.router.use(AuthMiddleware); for whole file
  }
}

export default AuthRoute;