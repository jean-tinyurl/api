import cors from "cors";
import express from "express";
import morgan from "morgan";
import {router} from "./router";

const app: express.Application = express();

app.use(cors());
/** Logging */
app.use(morgan("dev"));
/** Takes care of JSON data */
app.use(express.json());
/** Parse the request */
app.use(express.urlencoded({ extended: false }));

app.use(express.static("/api"));
console.log(__dirname);

// load router
for (const route of router) {
    app.use(route.getPrefix(), route.getRouter());
}

// app.use(AuthMiddleware); for global
module.exports = app;
