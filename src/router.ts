import Route from "./routes/route";
import AuthRoute from "./routes/auth.route";
import TinyUrlRoute from "./routes/tinyurl.route";

export const router: Array<Route> = [
    new AuthRoute(),
    new TinyUrlRoute(),
];