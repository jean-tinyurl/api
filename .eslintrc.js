module.exports = {
    parser:  "@typescript-eslint/parser", // ESLint parser
    extends: ["plugin:@typescript-eslint/recommended"], // sub-rule
    plugins: ["@typescript-eslint"], // dependencies
    env:{
        browser: true,
        node: true
    },
    rules: {
        // enable additional rules
        indent: ["error", 4],
        "linebreak-style": ["error", "unix"],
        quotes: ["error", "double"],
        semi: ["error", "always"],
    
        // override default options for rules from base configurations
        "comma-dangle": ["error", "never"],
        "no-cond-assign": ["error", "always"],
    
        // disable rules from base configurations
        "no-console": "off",
        "no-unused-vars": ["error"],

        "allowArgumentsExplicitlyTypedAsAny": 1,
        "typescript/explicit-function-return-type": [
		    "error",
            {
                "allowVoid": true
            }
        ]
    }                             
};